var assert = require('assert');
var TelegramBot = require('node-telegram-bot-api');
var request = require('request');
var moment = require('moment');
var cache = require('memory-cache');
var reddit = require('redwrap');
var teams = require('./assets/teams.json');
var playersByTeam = require('./assets/players-by-teams.json');
var playersByWebName = require('./assets/players-by-web-name.json');
var CronJob = require('cron').CronJob;
var Q = require("q");
var Firebase = require("firebase");

//amber-inferno-3799

var BOT_TOKEN = process.env.BOT_TOKEN;
var FIRE_BASE_APP = process.env.FIRE_BASE_APP;
var FIRE_BASE_ROOT = process.env.FIRE_BASE_ROOT;
var CHANNEL_NAME = process.env.CHANNEL_NAME;


var myFirebaseRef;
if(FIRE_BASE_APP && FIRE_BASE_ROOT){
  myFirebaseRef = new Firebase("https://" + FIRE_BASE_APP + ".firebaseio.com/" + FIRE_BASE_ROOT);
}

assert.ok(BOT_TOKEN, 'BOT_TOKEN is missing');
assert.ok(CHANNEL_NAME, 'CHANNEL_NAME is missing');
// Setup polling way
var bot = new TelegramBot(BOT_TOKEN, {polling: true});

function readFireBaseToCache()
{
  if(myFirebaseRef){

    var competitionRef = myFirebaseRef.child("PREMIER_LEAGUE");
    // Attach an asynchronous callback to read the data at our posts reference
    competitionRef.once("value", function(data) {
      console.log("Firebase call");
      cache.put("PREMIER_LEAGUE", data.val(), cacheTTL);
    });
  }
}

readFireBaseToCache();

function getUseage() {
  var teams = '*/teams* - list teams in premier league';
  var players = '*/players* _ARS_ - list players for team';
  var player = '*/player* _milner_ - gets premier league json for player (to be formatted)';
  var goals = '*/goals* - search reddit for latest goal videos';
  var tore = '*/tore* - search reddit for latest bundesliga goal videos';

  return  [
    teams,
    players,
    player,
    goals,
    tore
  ].reduce(function (last, match) {
    return last + ' \n ' + match;
  });
}

new CronJob('0 */1 * * * *', function() {
  console.log('Going to reddit to look for new goals');
  checkForBplGoals();
}, null, true, null);

var cacheTTL = 1000*60*60*24*7; // 1 week
var lastId;

bot.onText(/\/goals[ ]?(.*)/, function(msg, match){
    var chatId = msg.chat.id;

    if(match && match.length > 0 && match[1] !== "")
    {
      var timePeriod = match[1];
      getGoalsForTimePeriod(chatId, "PREMIER_LEAGUE", timePeriod);
    }
    else {
      var kb = {
          keyboard: [
              ['/goals today'],
              ['/goals yesterday'],
              ['/goals week']
          ],
          one_time_keyboard: true,
          selective : true
      };
      bot.sendMessage(chatId, "When do you want the goals from?", {reply_markup :kb,
                                                                    reply_to_message_id: msg.message_id
                                                                  });
    }

    // cache.keys().forEach(function (key) {
    //   var goal = cache.get(key);
    //   bot.sendMessage(chatId, goal.title + " " + goal.url);
    // });

});

var TIMESTAMP_MAP = {
  today : 1000*60*60*24*1,
  yesterday : 1000*60*60*24*2,
  week : 1000*60*60*24*7
}

function getDateFormatOfGoal(timestamp)
{
  var date = moment(timestamp, "X");
  return date.format("YY-MM-DD");
}

function storeGoal(goal, competition){
  var newGoal = false;
  var goalDate = getDateFormatOfGoal(goal.timestamp);
  var competitionGoals = cache.get(competition);

  if(!competitionGoals || !competitionGoals.goals)
  {
    competitionGoals = {
      goals : {}
    };
  }

  if(!competitionGoals.goals[goalDate])
  {
    competitionGoals.goals[goalDate] = {};
  }

  if(!competitionGoals.goals[goalDate][goal.id]){
    competitionGoals.goals[goalDate][goal.id] = goal;
    newGoal = true;
    cache.put(competition, competitionGoals, cacheTTL);
  }

  if(newGoal){
    if(myFirebaseRef){
      var dateRef = myFirebaseRef.child(competition).child("goals").child(goalDate);

      var goalRef = dateRef.child(goal.id);
      goalRef.set(goal);
    }

    bot.sendMessage(CHANNEL_NAME, goal.title + " " + goal.url);
  }
}

function convertTimePeriodToDates(timePeriod) {
  var days = [0];
  if ( timePeriod === 'yesterday') {
    days = [1];
  } else if (timePeriod === 'week') {
    days = [0,1,2,3,4,5,6];
  }
  return days.map(function formatDate(daysAgo) {
    return moment().subtract(daysAgo, 'days').format("YY-MM-DD");
  });
}

function getGoalsForTimePeriod(chatId, competition, timePeriod) {
  var dates = convertTimePeriodToDates(timePeriod);
  var goals = [];
  var competitionGoals = cache.get(competition);

  dates.forEach(function populateGoals(date) {
    var hasGoalsForDate = competitionGoals.goals && competitionGoals.goals[date];
    if (hasGoalsForDate) {
      goals.push.apply(goals, Object.keys(competitionGoals.goals[date]).map(function (goalKey) {
        return competitionGoals.goals[date][goalKey];
      }));
    }
  });
  // sort goals by date
  goals = goals.sort(function sortGoals(goala, goalb){
    return goala.timestamp - goalb.timestamp;
  });

  var hasGoals = goals.length > 0;
  if (hasGoals) {
    var promise = Q();

    goals.forEach(function(goal){
        promise = promise.then(function(){ return bot.sendMessage(chatId, goal.title + " " + goal.url); }); // or .bind
    });
  } else {
    bot.sendMessage(chatId, "Didn't find any goals for this time period.");
  }
}

function checkForBplGoals()
{
  reddit.r('soccer').sort('new').from("week").limit("100", function(err, data, res){
  data.data.children.forEach(function (child){
    var linkData = child.data;
    if(linkData.link_flair_text == "Media") {
      var re = /\d+\-\d+/;
      if(re.test(linkData.title))
      {
        if(checkForBplTeam(linkData.title)){

          storeGoal({ id : linkData.id,
                      title : linkData.title,
                      url : linkData.url,
                      timestamp : linkData.created_utc}, "PREMIER_LEAGUE");
        }
      }
    }

    //lastId = linkData.id;
  });
});
}

bot.onText(/\/force/, function (msg) {
  console.log('/force');
  checkForBplGoals();
});

bot.onText(/\/maybe/, function (msg) {
  console.log('/maybe');
  bot.sendMessage(msg.chat.id, process.env.VARIBL);
});

bot.onText(/\/help/, function (msg) {
  console.log('/help');
  bot.sendMessage(msg.chat.id, getUseage(), {
    parse_mode: 'Markdown'
  });
});

bot.onText(/\/start/, function (msg) {
  console.log('/start');
  bot.sendMessage(msg.chat.id, getUseage(), {
    parse_mode: 'Markdown'
  });
});



bot.onText(/\/checkargos (.+)/, function (msg, match) {
  var chatId = msg.chat.id;
  request('http://www.checkargos.com/stockcheck/' + match[1], function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var status = JSON.parse(body);
      bot.sendMessage(chatId, 'in stock ' + status.isStocked );
    } else {
      bot.sendMessage(chatId, "bad bot");
    }
  });
});

bot.onText(/\/today/, function (msg, match) {
  console.log('/today');
  request('http://football-api.com/api/?Action=today&APIKey=c9e32b20-f7da-b655-8a24a0c88f13', function (error, response, body) {
    processMatchesResponse(msg, 'today', error, response, body);
  });
});

bot.onText(/\/tomorrow/, function (msg, match) {
  console.log('/tomorrow');
  var today = new Date();
  var fromDate = moment(today).format('DD.MM.YYYY');
  var toDate = moment(today).add(1, 'd').format('DD.MM.YYYY');
  request('http://football-api.com/api/?Action=fixtures&APIKey=c9e32b20-f7da-b655-8a24a0c88f13' + '&from_date=' + fromDate + '&to_date=' + toDate, function (error, response, body) {
    processMatchesResponse(msg, 'tomorrow', error, response, body);
  });
});

bot.onText(/\players (.+)/, function (msg, match) {
  console.log('/players' + match[1]);
  var teamPlayers = playersByTeam[match[1].toUpperCase()].map(function (player) {
    return '/player ' + player.web_name + ' ' + player.type_name[0];
  }).reduce(function (last, next) {
    return last + '\n' + next;
  });

  bot.sendMessage(msg.chat.id, match[1].toUpperCase() + '\n' + teamPlayers);
});

bot.onText(/\/player (.+)/, function (msg, match) {
  console.log('/player');
  console.log(match[1]);
  var id = playersByWebName[match[1].toUpperCase()].id;
  var url = 'http://fantasy.premierleague.com/web/api/elements/' + id + '/';
  console.log(url);
  request(url, function (error, response, body) {
    bot.sendMessage(msg.chat.id, 'Player: \n' + body);
  });

});

bot.onText(/\/teams/, function (msg) {
  console.log('/teams');
  bot.sendMessage(msg.chat.id, 'Teams available: \n' + Object.keys(teams).map(function (team) { return '/players ' + team; }).reduce(function (last, team) {
    return last + ' \n ' + team;
  }));
});

bot.onText(/\/week/, function (msg, match) {
  console.log('/week');
  var today = new Date();
  var fromDate = moment(today).format('DD.MM.YYYY');
  var toDate = moment(today).add(1, 'w').format('DD.MM.YYYY');
  request('http://football-api.com/api/?Action=fixtures&APIKey=c9e32b20-f7da-b655-8a24a0c88f13' + '&from_date=' + fromDate + '&to_date=' + toDate, function (error, response, body) {
    processMatchesResponse(msg, 'week', error, response, body);
  });
});

bot.onText(/\/tore/, function(msg, match){
    var chatId = msg.chat.id;
    var bplOnly = false;
    reddit.r('bundesliga').sort('new').from("week").limit("10", function(err, data, res){
    data.data.children.forEach(function (child){
      var linkData = child.data;
      console.log(linkData);
      var re = /[\(\[]\d+\-\d+[\)\]]/;
      if(re.test(linkData.title))
      {
        if(bplOnly)
        {
          if(checkForBplTeam(linkData.title)){
            bot.sendMessage(chatId, linkData.title + " " + linkData.url);
          }
        }
        else {
            bot.sendMessage(chatId, linkData.title + " " + linkData.url);
        }
      }
    });
  });
});

var BPLTEAMS = [
  "ASTON VILLA",
  "ARSENAL",
  "BOURNEMOUTH",
  "CHELSEA",
  "CRYSTAL PALACE",
  "EVERTON",
  "LEICESTER",
  "LIVERPOOL",
  "MANCHESTER",
  "NEWCASTLE",
  "NORWICH",
  "SOUTHAMPTON",
  "STOKE",
  "SUNDERLAND",
  "SWANSEA",
  "TOTTENHAM",
  "WATFORD",
  "WEST BROM",
  "WEST HAM"
];

function checkForBplTeam(linkText)
{
  var bplTeam = false;
  BPLTEAMS.forEach(function (team){
    if(linkText.toUpperCase().indexOf(team) > 0)
    {
      bplTeam = true;
      return;
    }
  });

  return bplTeam;
}

function parseMatches(matches) {
  return matches.map(function (match) {
    return ' ' + match.match_date + ' *' +  match.match_time + '* ' + match.match_localteam_name + ' v ' + match.match_visitorteam_name;
  }).reduce(function (last, match) {
    return last + ' \n ' + match;
  });
}

function processMatchesResponse(msg, timeframe, error, response, body) {
  if (!error && response.statusCode == 200) {
    var status = JSON.parse(body);
    if (status.matches !== undefined) {
      bot.sendMessage(msg.chat.id, parseMatches(status.matches), {
        parse_mode: 'Markdown'
      });
    } else {
      bot.sendMessage(msg.chat.id, 'Did not find any matches for ' + timeframe);
    }

  } else {
    bot.sendMessage(msg.chat.id, "bad bot");
  }
}
