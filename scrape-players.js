var jsonfile = require('jsonfile');
var request = require('sync-request');
var pla = require('./assets/pla.json');
var teams = require('./assets/teams.json');
var teamCodes = {};

Object.keys(teams).forEach(function (teamShortName) {
  teamCodes[teams[teamShortName].code] = teamShortName;
});

console.log(teamCodes);

// var playersByTeam = {};
var playersByWebName = {};

for (var playerId in pla) {
  var player = pla[playerId];
  var essential = {
    "id": player.id,
    "web_name": player.web_name,
    "team_code": player.team_code,
    "team_id": player.team_id,
    "type_name": player.type_name
  };

  // playersByTeam[teamCodes[player.team_code]] = playersByTeam[teamCodes[player.team_code]] || [];
  // playersByTeam[teamCodes[player.team_code]].push(essential);

  playersByWebName[essential.web_name.toUpperCase()] = playersByWebName[essential.web_name.toUpperCase()] || essential;
}

// jsonfile.writeFile('./assets/players-by-teams.json', playersByTeam, {spaces: 2}, function(err) {
//   console.error(err);
// });

jsonfile.writeFile('./assets/players-by-web-name.json', playersByWebName, {spaces: 2}, function(err) {
  console.error(err);
});


// for (var i = 1; i <= 630; i++ ) {
//   var url = 'http://fantasy.premierleague.com/web/api/elements/' + i + '/';
//   var res = request('GET', url);
//   console.log(i);
//   players[i] = JSON.parse(res.getBody('utf8'));
// }

// jsonfile.writeFile('./assets/pla.json', players, {spaces: 2}, function(err) {
//   console.error(err)
// });
